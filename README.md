# GreenerConsumption


## Integrate with your tools

- [ ] [Set up project integrations](https://gricad-gitlab.univ-grenoble-alpes.fr/imardv/greenerconsumption/-/settings/integrations)

## Description
Projet simple pour la prédiction de la consommation de GreEn-ER.   
Le dossier data contient :  
* un jeu de données d'entrainement de la forme (Date, Consommation du batiment, Température Extérieure) du 01/01/2017 au 30/06/2019 par pas d'une heure
* un jeu de données de test de la forme (Date, Consommation du batiment, Température Extérieure) du 01/07/2019 au 31/12/2019 par pas d'une heure

Les données ne contiennent pas de valeurs abérantes mais des NaN sont présents et sont remplacés avec une interpolation polynomiale d'ordre 2. 


Le script train.py permet d'entrainer le modèle sur le jeu de données d'entrainement et de le sauvegarder.  
Le fichier validation.ipynb permet de charger le modèle sauvegarder et de le valider en utilisant le jeu de données de test.  
Il affiche ensuite différentes métriques de régression : mse, mae, rmse, rmsle, me  
et affiche ensuite un graphe affichant la courbe prédite, la courbe réelle, la superposition des deux et la courbe d'erreur absolue.  

## Installation
Pour l'installation des dépendances il suffit de lancer la commande : 
```pip install -r requirements.txt```
