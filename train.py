import numpy as np
import tensorflow as tf

import matplotlib.pyplot as plt
import pandas as pd

from utils.data_processing import normalize, to_timeSeries

'''
Création d'un modèle LSTM pour l'exemple.
'''
def RNNmodel_fn(input_seq_len, output_len, hidden=256):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Reshape((1, input_seq_len), input_shape=(input_seq_len, )))
    model.add(tf.keras.layers.LSTM(hidden))
    model.add(tf.keras.layers.Dense(output_len, activation='relu'))

    model.compile(optimizer='rmsprop', loss='mse')

    return model


def train():
    
    df = pd.read_csv('data/training_data_2017_2019.csv', parse_dates=['Date'], index_col='Date')
    df = df.interpolate(method='polynomial', order=2)

    normalized_df = normalize(df)


    '''
    On crée le jeu de données pour le fit. 
    Cette partie est à changer en fonction du modèle.
    '''
    conso_df = normalized_df['Global Consumption']
    X, y = to_timeSeries(conso_df.to_numpy())


    model_1 = RNNmodel_fn(12, 1, hidden=128)
    history = model_1.fit(X, y, epochs=10, batch_size=64)

    #Creer le dossier pretrained automatiquement, le nom est à changer
    model_1.save('pretrained/test_model.h5')


if __name__ == "__main__":
    train() 
