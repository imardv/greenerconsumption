import numpy as np
import pandas as pd


def to_timeSeries(dataset, look_back=12, look_forward=1, delay=1, type='seq2point'):
    dataX, dataY = [], []
    if type == 'seq2seq':
        for i in range(look_back, len(dataset)-look_back-look_forward-delay):
            a = dataset[i-look_back:i].flatten()
            dataX.append(a)
            dataY.append(dataset[i+delay:(i+look_back + look_forward)].flatten())
        return np.array(dataX, dtype=object).astype('float32'), np.array(dataY, dtype=float).astype('float32')
    elif type == 'seq2point':
        for i in range(look_back, len(dataset)-look_back-1):
            a = dataset[i-look_back:i].flatten()
            dataX.append(a)
            dataY.append(dataset[i - look_back + 1].flatten())
        return np.array(dataX, dtype=object).astype('float32'), np.array(dataY, dtype=float).astype('float32')


def normalize(df):
    new_df = df.copy()
    for col in new_df.columns:
        new_df[col] = (new_df[col] - new_df[col].min()) / (new_df[col].max() - new_df[col].min())
    return new_df