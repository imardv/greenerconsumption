from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_absolute_percentage_error, max_error, mean_squared_log_error
import numpy as np
import matplotlib.pyplot as plt


def regression_metrics(predictions, y, metrics):
    metrics_results = dict()
    if 'mse' in metrics:
        metrics_results['mse'] = mean_squared_error(y, predictions)
    if 'mae' in metrics:
        metrics_results['mae'] = mean_absolute_error(y, predictions)
    if 'rmse' in metrics:
        metrics_results['rmse'] = np.sqrt(mean_squared_error(y, predictions))
    if 'rmsle' in metrics:
        metrics_results['rmsle'] = mean_squared_log_error(y, predictions)
    if 'me' in metrics:
        metrics_results['me'] = max_error(y, predictions)

    return metrics_results


def compare(prediction, y, figsize=(15, 9)):
    fig, ax = plt.subplots(4, 1, figsize=figsize)
    
    ax[0].plot(prediction, label='predicted')
    ax[0].legend()
    
    ax[1].plot(y, label='real')
    ax[1].legend()
    
    ax[2].plot(prediction, label='predicted')
    ax[2].plot(y, label='real')
    ax[2].legend()
    
    ax[3].plot(np.abs(prediction - y), label='absolute error')
    ax[3].legend()
    
    return fig